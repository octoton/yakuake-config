#!/usr/bin/env bash

qdbus 2> /dev/null | grep -q org.kde.yakuake &> /dev/null
if [ $? != "0" ]; then
	kdialog --error "Yakuake is not running! Run it before." &> /dev/null
	exit 1
fi

[[ "$1" != "" ]] && dir="$1" || dir=`pwd`
dir=$(realpath "$dir")

{
	newSession=$(qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.addSession)

	qdbus org.kde.yakuake /yakuake/tabs org.kde.yakuake.setTabTitle $newSession "$(basename "$dir")"

	terminalID=$(qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.terminalIdsForSessionId $newSession)

	qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.runCommand "$1"

	qdbus org.kde.yakuake /yakuake/window toggleWindowState
} &> /dev/null